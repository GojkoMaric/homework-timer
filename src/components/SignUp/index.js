import React, { Component } from 'react'

class SignUp extends Component {
    constructor() {
        super()
        this.state = {
            username: '',
            password: '',
            termsAgreed: false,
            newUsername: '',
            newPassword: '',
        }
    }

    handleUsername = (e) =>{
        this.setState({
            newUsername: e.target.value
        })
    }

    handlePassword = (e) => {
        this.setState({
            newPassword: e.target.value
        })
    }

    handlePrivacy = (e) => {
        this.setState({
            termsAgreed: e.target.checked
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({
            username: this.state.newUsername,
            password: this.state.newPassword,
            newPassword: '',
            newUsername: ''
        })
    }
    
    render() {
        const validationError = <span className="error">Please fill this field</span>;
        return (
            <div className="sign-up-form-wrapper">
                <form className="sign-up-form" onSubmit={this.handleSubmit}>
                    <div className="input-wrapper">
                        <label>Username:</label>
                        <input name="username"
                        type="text"
                        onChange={this.handleUsername}
                        value={this.state.newUsername}/>
                        { !this.state.newUsername && validationError}
                    </div>
                    <div className="input-wrapper">
                        <label>Password:</label>
                        <input name="password"
                        type="password"
                        onChange={this.handlePassword}
                        value={this.state.newPassword}/>
                        { !this.state.newPassword && validationError }
                    </div>
                    <div className="checkbox-wrapper">
                        <input type="checkbox"
                        onChange={this.handlePrivacy}
                        value={this.state.termsAgreed}/>
                        <span>I agree with terms and conditions...</span>
                    </div>
                    <button 
                    disabled={ !this.state.newUsername || !this.state.newPassword || !this.state.termsAgreed}
                    type="submit">SignUp</button>
                </form>
            </div>
        )
    }
}

export default SignUp;