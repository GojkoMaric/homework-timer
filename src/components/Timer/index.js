import React, { Component } from "react"
import SignUp from "../SignUp";

class Timer extends Component {

    constructor() {
        super()
        this.state = {
            seconds: 0,
        }
    }

    timerCounter() {
        let timerSeconds = 0;

        setInterval(
            () => {
                timerSeconds++;
                
                this.setState({
                    seconds: timerSeconds
                })
            },
            1000
        )
    }

    componentDidMount() {
        this.timerCounter();
    }

    componentDidUpdate = () => {

        this.state.seconds <= 60 && clearInterval(this.timerCounter);
        this.state.seconds >= 60 && console.log("Timer Clear");
    }

    render () {
        const signUpForm = this.state.seconds <= 60 ? <SignUp /> : <p>Sorry, your time is up...</p>

        return (
            <div className="timer-wrapper">
                <h4>Timer</h4>
                <p>You have 60 seconds to submit your form</p>
                <p>Seconds passed: {this.state.seconds <= 60 && this.state.seconds}</p>
                { signUpForm }
            </div>
        )
    }
}

export default Timer