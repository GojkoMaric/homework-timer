import React from 'react';
import Timer from './components/Timer'
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h3 className="appTitle">Timer Homework</h3>
      </header>
      <Timer />
    </div>
  );
}

export default App;
